Service for entity spotting using bert and trained on the wikiner corpus.
This instance is for English only (took out the language parameter in the flask controller, and links in a hardcoded way to nermodels/wikiner-en for the trained/tuned bert model).
