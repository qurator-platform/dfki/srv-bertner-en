#!/usr/bin/python3
from flask import Flask, Response, flash, redirect, request, url_for
from flask_cors import CORS
import os
import json
from werkzeug.utils import secure_filename

# custom modules
import ner

"""
to start run:
export FLASK_APP=flaskController.py
export FLASK_DEBUG=1 (optional, to reload upon changes automatically)
python -m flask run

example calls:

curl -X GET localhost:5000/detectLanguages?input="aap"
curl -F 'pdffile=@temp_pdf_storage/machine_readable_single_column_2.pdf' -X POST localhost:5000/ocr
"""

app = Flask(__name__)
app.secret_key = "super secret key"
CORS(app)

@app.route('/welcome', methods=['GET'])
def dummy():
    return "Hello stranger, can you tell us where you've been?\nMore importantly, how ever did you come to be here?\n"

##################### entity spotting #####################
@app.route('/spotEntities', methods=['GET'])
def spotEntities():

    """
    Required args:
    - informat (turtle, txt, etc.)
    - outformat (turtle, other nif-usuals)
    - input (actual input text to spot entities in)
    """
    
    supported_informats = ['txt']
    supported_informats.extend(ner.SUPPORTED_NIFFORMATS)
    supported_outformats = ner.SUPPORTED_NIFFORMATS
    inp = None
    if request.args.get('input') == None:
        if request.data == None:
            return 'Please provide some text as input.\n'
        else:
            inp = request.data.decode('utf-8')
    else:
        inp = request.args.get('input')
    if request.args.get('informat') == None:
        return 'Please specify input format (currently supported: %s)\n' % str(supported_informats)
    elif request.args.get('informat') not in supported_informats:
        return 'Input format "%s" not among supported formats. Please picke one from: %s.\n' % (request.args.get('informat'), (str(supported_informats)))
    if request.args.get('outformat') == None:
        return 'Please specify output format (currently supported: %s)\n' % str(supported_outformats)
    elif request.args.get('outformat') not in supported_outformats:
        return 'Output format "%s" not among supported formats. Please picke one from: %s.\n' % (request.args.get('outformat'), (str(supported_outformats)))

    
    informat = request.args.get('informat')
    outformat = request.args.get('outformat')
    
    result = ner.predict(inp, informat, outformat)    
    
    return result


def create_error(code, message):
    failure_response_dict = {
        "failure": {
            "errors": [{
                "code": code,
                "text": message
            }]
        }
    }
    return Response(response=json.dumps(failure_response_dict), status=500, content_type='application/json')


##################### entity spotting #####################
@app.route('/', methods=['POST'])
def spotEntitiesELG():
    if request.method == 'POST':
        ctype = request.headers["Content-Type"]
        print("Content type: {}".format(ctype))
        accept = request.headers["Accept"]
        print("Response type: {}".format(accept))

        if not request.data:
            return create_error("elg.request.missing",
                                "Error: No request provided in message")

        request_body = request.data.decode('utf-8')
        request_dict = json.loads(request_body)
        mimetype = request_dict['mimeType']

        if mimetype == 'text/plain':
            content = request_dict['content']
            print('Content: {}'.format(content))
        else:
            return create_error("elg.request.text.mimeType.unsupported",
                                "Error: MIME type {} not supported by this service".format(mimetype))

        if 'application/json' in accept:
            pass
        else:
            return create_error("elg.request.type.unsupported",
                                "ERROR: Request type {} not supported by this service".format(accept))

        if ctype == 'application/json':
            ner_annotations = ner.predictELG(content)
            response_dict = {
                "response": {
                    "type": "texts",
                    "texts": [{
                        "content": content,
                        "annotations": {
                            "Person": ner_annotations['PER'],
                            "Location": ner_annotations['LOC'],
                            "Organisation": ner_annotations['ORG'],
                            "Miscellaneous": ner_annotations['MISC']
                        }
                    }]
                }
            }
            return Response(response=json.dumps(response_dict), status=200, content_type='application/json')
        else:
            return create_error("elg.request.type.unsupported",
                                'Request type {} not supported by this service'.format(ctype))
    else:
        return create_error("elg.request.invalid",
                            'Invalid request message')


if __name__ == '__main__':
    port = int(os.environ.get('PORT', 8080))
    app.run(host='localhost', port=port, debug=True)
