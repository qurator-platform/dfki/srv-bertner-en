Service for entity spotting. Based on BERT (bert-base-cased) and trained on the WikiNER corpus (Nothman et al., 2012).

Note that this code works for English only.
As input it expects either plaintext or NIF in n3/turtle format, and outputs NIF in n3/turtle format (turtle, rdf-xml or json-ld)