from nltk import sent_tokenize, word_tokenize
import spacy
from spacy.lang.en import English
import sys

nlp = English()
tokenizer = nlp.Defaults.create_tokenizer(nlp)

def checkOffsets(isstr, token, start, end):

    
    substr = isstr[start:end]
    if not substr == token:
        sys.stderr.write('ERROR: Idexing is off. Token points at "%s" while isstr[%i:%i] points at "%s". This will result in faulty character offsets. Dying now!\n' % (token, start, end, substr))
        sys.exit(1)
    

def addOffsets(isstr):

    tokenoffsets = []
    offset = 0
    sentences = sent_tokenize(isstr) # make sure that this is in line with whatever the backend (ner.py in my original case) is doing to process the entire text (i.e. this may use another procedure for sentence splitting or tokenisation)

    for si, sentence in enumerate(sentences):
        tokens = word_tokenize(sentence)
        #tokens = [str(x) for x in tokenizer(sentence)] # if we run into the issue with nltk replacing double quotes again, this is how the spacy tokenizer can be used instead. Comment out line above and activate this one.
        tls = len(tokens)
        for ti, token in enumerate(tokens):
            if ti == 0 and not sentence.startswith(token): # this is to check for initial whitespaces
                offset += sentence.find(token)
            tl = len(token)
            start, end = offset, offset+tl
            checkOffsets(isstr, token, start, end) # in place method to check and crash when indexing goes off (may want to comment this out when I'm positive this works in all cases)
            tokenoffsets.append((start, end))
            offset += tl
            if ti < tls-1:
                # getting space in between current and next token here
                offset += isstr[end:].find(tokens[ti+1])
        
        if si < len(sentences)-1:
            # getting length of trailing stuff here
            offset += isstr[offset:].find(sentences[si+1])

    return tokenoffsets
