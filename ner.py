# coding=utf-8
import os
import re
import sys
import torch
import codecs
import json
import random
import numpy as np
import configparser
from tqdm import tqdm, trange
from seqeval.metrics import classification_report
import utils

from torch.utils.data.distributed import DistributedSampler
from torch.utils.data import (DataLoader, RandomSampler, SequentialSampler, TensorDataset)
from pytorch_pretrained_bert.tokenization import BertTokenizer
from torch import nn
from torch.nn import CrossEntropyLoss
from pytorch_pretrained_bert.optimization import BertAdam, WarmupLinearSchedule
from pytorch_pretrained_bert.file_utils import PYTORCH_PRETRAINED_BERT_CACHE
from pytorch_pretrained_bert.modeling import (CONFIG_NAME, WEIGHTS_NAME, BertConfig, BertForTokenClassification)

from bert import Ner
from nif import Nif

SUPPORTED_NIFFORMATS = ['turtle', 'xml', 'json-ld']

config = configparser.ConfigParser()
config.read('config.ini')
global_step=0
nb_tr_steps=0
tr_loss=0
sys.stderr.write('INFO: Device: %s\n' % config['general']['device_type'])
if config['general']['device_type'] == 'cuda':
    os.environ["CUDA_VISIBLE_DEVICES"]=config['general']['gpu_nr']
    sys.stderr.write('INFO: Cuda visible devices: %s\n' % config['general']['gpu_nr'])

OUTPUT_DIR = config['locations']['output_root_folder']
if not os.path.exists(OUTPUT_DIR):
    os.makedirs(OUTPUT_DIR)


def readfile(filename):
    f = codecs.open(filename)
    data, sentence, label = [], [], []
    for line in f:
        if len(line)==0 or line.startswith('-DOCSTART') or line[0]=="\n":
            if len(sentence) > 0:
                data.append((sentence,label))
                sentence, label = [], []
            continue
        parts = line.split(' ')
        sentence.append(parts[0])
        
        label.append(parts[-1][:-1])

    if len(sentence) >0:
        data.append((sentence,label))
        sentence, label = [], []
    return data

def convert_examples_to_features(examples, label_list, tokenizer):
    label_map = {label : i for i, label in enumerate(label_list,1)}

    features = []
    for (ex_index,example) in tqdm(enumerate(examples)):
        textlist = example.text_a.split(' ')
        labellist = example.label
        tokens = []
        labels = []
        valid = []
        label_mask = []
        for i, word in enumerate(textlist):
            token = tokenizer.tokenize(word)
            tokens.extend(token)
            label_1 = labellist[i]
            for m in range(len(token)):
                if m == 0:
                    labels.append(label_1)
                    valid.append(1)
                    label_mask.append(1)
                else:
                    valid.append(0)
        if len(tokens) >= int(config['params']['max_seq_length']) - 1:
            tokens = tokens[0:(int(config['params']['max_seq_length']) - 2)]
            labels = labels[0:(int(config['params']['max_seq_length']) - 2)]
            valid = valid[0:(int(config['params']['max_seq_length']) - 2)]
            label_mask = label_mask[0:(int(config['params']['max_seq_length']) - 2)]
        ntokens = []
        segment_ids = []
        label_ids = []
        ntokens.append("[CLS]")
        segment_ids.append(0)
        valid.insert(0,1)
        label_mask.insert(0,1)
        label_ids.append(label_map["[CLS]"])
        for i, token in enumerate(tokens):
            ntokens.append(token)
            segment_ids.append(0)
            if len(labels) > i:
                label_ids.append(label_map[labels[i]])
        ntokens.append("[SEP]")
        segment_ids.append(0)
        valid.append(1)
        label_mask.append(1)
        label_ids.append(label_map["[SEP]"])
        input_ids = tokenizer.convert_tokens_to_ids(ntokens)
        input_mask = [1] * len(input_ids)
        label_mask = [1] * len(label_ids)
        while len(input_ids) < int(config['params']['max_seq_length']):
            input_ids.append(0)
            input_mask.append(0)
            segment_ids.append(0)
            label_ids.append(0)
            valid.append(1)
            label_mask.append(0)
        while len(label_ids) < int(config['params']['max_seq_length']):
            label_ids.append(0)
            label_mask.append(0)
        assert len(input_ids) == int(config['params']['max_seq_length'])
        assert len(input_mask) == int(config['params']['max_seq_length'])
        assert len(segment_ids) == int(config['params']['max_seq_length'])
        assert len(label_ids) == int(config['params']['max_seq_length'])
        assert len(valid) == int(config['params']['max_seq_length'])
        assert len(label_mask) == int(config['params']['max_seq_length'])

        features.append(InputFeatures(input_ids=input_ids,
                                      input_mask=input_mask,
                                      segment_ids=segment_ids,
                                      label_id=label_ids,
                                      valid_ids=valid,
                                      label_mask=label_mask))
    return features

class InputExample(object):
    def __init__(self, guid, text_a, text_b=None, label=None):
        """Constructs a InputExample.
        Args:
            guid: Unique id for the example.
            text_a: string. The untokenized text of the first sequence. For single
            sequence tasks, only this sequence must be specified.
            text_b: (Optional) string. The untokenized text of the second sequence.
            Only must be specified for sequence pair tasks.
            label: (Optional) string. The label of the example. This should be
            specified for train and dev examples, but not for test examples.
        """
        self.guid = guid
        self.text_a = text_a
        self.text_b = text_b
        self.label = label


class InputFeatures(object):
    def __init__(self, input_ids, input_mask, segment_ids, label_id, valid_ids=None, label_mask=None):
        self.input_ids = input_ids
        self.input_mask = input_mask
        self.segment_ids = segment_ids
        self.label_id = label_id
        self.valid_ids = valid_ids
        self.label_mask = label_mask

class DataProcessor(object):
    def get_train_examples(self, data_dir):
        raise NotImplementedError()

    def get_dev_examples(self, data_dir):
        raise NotImplementedError()

    def get_labels(self):
        raise NotImplementedError()

    @classmethod
    def _read_tsv(cls, input_file, quotechar=None):
        return readfile(input_file)


class NerProcessor(DataProcessor):
    def get_instances(self, tsvfile, target): # target should be one of [train,test,dev]
        return self._create_examples(self._read_tsv(tsvfile), target)
    
    def get_labels(self): # may want to modify this at some point
        return ["O", "B-MISC", "I-MISC",  "B-PER", "I-PER", "B-ORG", "I-ORG", "B-LOC", "I-LOC", "[CLS]", "[SEP]"]

    def _create_examples(self,lines,set_type):
        examples = []
        for i,(sentence,label) in enumerate(lines):
            guid = "%s-%s" % (set_type, i)
            text_a = ' '.join(sentence)
            text_b = None
            label = label
            examples.append(InputExample(guid=guid,text_a=text_a,text_b=text_b,label=label))
        return examples

class Ner(BertForTokenClassification):
   
    def forward(self, input_ids, token_type_ids=None, attention_mask=None, labels=None,valid_ids=None,attention_mask_label=None):
        sequence_output, _ = self.bert(input_ids, token_type_ids, attention_mask, output_all_encoded_layers=False)
        batch_size,max_len,feat_dim = sequence_output.shape
        valid_output = torch.zeros(batch_size,max_len,feat_dim,dtype=torch.float32,device=config['general']['device_type'])
        for i in range(batch_size):
            jj = -1
            for j in range(max_len):
                    if valid_ids[i][j].item() == 1:
                        jj += 1
                        valid_output[i][jj] = sequence_output[i][j]
        sequence_output = self.dropout(valid_output)
        logits = self.classifier(sequence_output)

        if labels is not None:
            loss_fct = CrossEntropyLoss(ignore_index=0)
            # Only keep active parts of the loss
            attention_mask_label = None
            if attention_mask_label is not None:
                active_loss = attention_mask_label.view(-1) == 1
                active_logits = logits.view(-1, self.num_labels)[active_loss]
                active_labels = labels.view(-1)[active_loss]
                loss = loss_fct(active_logits, active_labels)
            else:
                loss = loss_fct(logits.view(-1, self.num_labels), labels.view(-1))
            return loss
        else:
            return logits

    
def train(traindataloc, modelname):

    device = torch.device(config['general']['device_type'])

    seed = 42
    random.seed(seed)
    np.random.seed(seed)
    torch.manual_seed(seed)

    nerp = NerProcessor()
    labels = nerp.get_labels()
    num_labels = len(labels) + 1
                  
    tokenizer = BertTokenizer.from_pretrained(config['locations']['bert_model_loc'])

    cache_dir = PYTORCH_PRETRAINED_BERT_CACHE
    model = Ner.from_pretrained(config['locations']['bert_model_loc'],cache_dir=cache_dir,num_labels = num_labels)
    model.to(device)
    param_optimizer = list(model.named_parameters())
    no_decay = ['bias', 'LayerNorm.bias', 'LayerNorm.weight']
    optimizer_grouped_parameters = [
        {'params': [p for n, p in param_optimizer if not any(nd in n for nd in no_decay)], 'weight_decay': 0.01},
        {'params': [p for n, p in param_optimizer if any(nd in n for nd in no_decay)], 'weight_decay': 0.0}
        ]


    train_instances = nerp.get_instances(traindataloc, 'train')
    sys.stderr.write('INFO: starting training with %s\n' % traindataloc)

    num_train_optimization_steps = int(len(train_instances) / int(config['params']['train_batch_size']) / int(config['params']['gradient_accumulation_steps'])) * float(config['params']['num_train_epochs'])

    
    optimizer = BertAdam(optimizer_grouped_parameters,
                         lr=float(config['params']['learning_rate']),
                         warmup=float(config['params']['warmup_proportion']),
                         t_total=num_train_optimization_steps)

    label_map = {i : label for i, label in enumerate(labels,1)}


    sys.stderr.write('INFO: Starting conversion of data\n')
    train_features = convert_examples_to_features(train_instances, labels, tokenizer)
    #sys.stderr.write('INFO: Done with conversion\n')
    
    all_input_ids = torch.tensor([f.input_ids for f in train_features], dtype=torch.long)
    all_input_mask = torch.tensor([f.input_mask for f in train_features], dtype=torch.long)
    all_segment_ids = torch.tensor([f.segment_ids for f in train_features], dtype=torch.long)
    all_label_ids = torch.tensor([f.label_id for f in train_features], dtype=torch.long)
    all_valid_ids = torch.tensor([f.valid_ids for f in train_features], dtype=torch.long)
    all_lmask_ids = torch.tensor([f.label_mask for f in train_features], dtype=torch.long)
    train_data = TensorDataset(all_input_ids, all_input_mask, all_segment_ids, all_label_ids,all_valid_ids,all_lmask_ids)
    train_sampler = RandomSampler(train_data)
    train_dataloader = DataLoader(train_data, sampler=train_sampler, batch_size=int(config['params']['train_batch_size']))

    model.train()
    
    global global_step
    for _ in trange(int(config['params']['num_train_epochs']), desc="Epoch"):
        tr_loss = 0
        nb_tr_examples, nb_tr_steps = 0, 0
        for step, batch in enumerate(tqdm(train_dataloader, desc="Iteration")):
            batch = tuple(t.to(device) for t in batch)
            input_ids, input_mask, segment_ids, label_ids, valid_ids,l_mask = batch
            loss = model(input_ids, segment_ids, input_mask, label_ids,valid_ids,l_mask)
            loss.backward()
            tr_loss += loss.item()
            nb_tr_examples += input_ids.size(0)
            nb_tr_steps += 1
            if (step + 1) % int(config['params']['gradient_accumulation_steps']) == 0:
                optimizer.step()
                optimizer.zero_grad()
                global_step += 1
    

    # Save a trained model and the associated configuration
    
    model_to_save = model.module if hasattr(model, 'module') else model  # Only save the model itself
    model_out_dir = os.path.join(OUTPUT_DIR, modelname)
    if not os.path.exists(model_out_dir):
        os.makedirs(model_out_dir)
    output_model_file = os.path.join(model_out_dir, WEIGHTS_NAME)
    torch.save(model_to_save.state_dict(), output_model_file)
    output_config_file = os.path.join(model_out_dir, CONFIG_NAME)
    with codecs.open(output_config_file, 'w') as f:
            f.write(model_to_save.config.to_json_string())
    label_map = {i : label for i, label in enumerate(labels,1)}    
    model_config = {"bert_model":config['locations']['bert_model_loc'],
                    "max_seq_length":int(config['params']['max_seq_length']),
                    "num_labels":len(labels)+1,
                    "label_map":label_map}
    json.dump(model_config,open(os.path.join(model_out_dir,"model_config.json"),"w"))
    
    sys.stderr.write('INFO: Trained model, saved to %s\n' % output_config_file)


def predict(inp, informat, outformat):

    # note to self: think the pretrained bert model file (which one from bert-base-cased, bert-base-german-cased or bert-base-multi-cased) is only important/used during training
    nermodelloc = os.path.join(OUTPUT_DIR, 'wikiner-en')#%s' % language)
    if not os.path.exists(nermodelloc):
        sys.stderr.write('ERROR: Model "%s" not found. Check config.ini settings. Dying now.\n' % nermodelloc)
        sys.exit()

    from bert import Ner

    nermodel = Ner(nermodelloc)

    nifmodel = Nif()

    # replacing de quotes, as nltk word_tokenizer translates these to `` (two characters; leading the nif indices to go off)
    #print('deb inform:', informat)
    if informat == 'txt':
        inp = re.sub('"', '\'', inp)
        nifmodel.initNifModelFromString(inp)
    elif informat in SUPPORTED_NIFFORMATS:
        nifmodel.parseModelFromString(inp, informat)
        #nifmodel.replaceDoubleQuotes() # this does not work! If someone complains about this; code is half-finished inside nif.py, continue to fix it! (PB, 05-09-2019)
    else:
        nifmodel.initNifModelFromString(inp)
        sys.stderr.write('ERROR: Input format "%s" not supported. Interpreting input as plaintext.\n' % informat)

    plaintextinput = nifmodel.extractIsString()

    nerp = NerProcessor()
    labelVocab = nerp.get_labels()
    nifmodel.addEntityLabelVocab(labelVocab)
    outputlabels = nermodel.predict(plaintextinput)

    sys.stderr.write('Debugging output labels:%s\n' % str(outputlabels))
    nifmodel.addEntities(outputlabels)

    nifString = None
    if outformat in SUPPORTED_NIFFORMATS:
        nifString = nifmodel.model.serialize(format=outformat).decode('utf-8')
    else:
        sys.stderr.write('ERROR: "turtle" is the only supported output format for now. Returning None, will probably crash afterwards...\n')
    
    return nifString
    
def predictELG(inp):
    nermodelloc = os.path.join(OUTPUT_DIR, 'wikiner-en')#%s' % language)
    if not os.path.exists(nermodelloc):
        sys.stderr.write('ERROR: Model "%s" not found. Check config.ini settings. Dying now.\n' % nermodelloc)
        sys.exit()
    from bert import Ner
    nermodel = Ner(nermodelloc)
    nifmodel = Nif()
    inp = re.sub('"', '\'', inp)
    plaintextinput = inp
    nerp = NerProcessor()
    labelVocab = nerp.get_labels()
    outputlabels = nermodel.predict(plaintextinput)
    entityLabelVocab = [x for x in labelVocab if not re.match('O', x) and not re.match('\[CLS\]', x) and not re.match('\[SEP\]', x)]

    tokenoffsets = utils.addOffsets(inp)
    #print(labels)
    entities = []
    zipiter = iter(zip(tokenoffsets, outputlabels))
    index = 0
    for tupl in zipiter:
        indices, labeldict = tupl[0], tupl[1]
        skip = 0
        if labeldict['tag'] in entityLabelVocab:
            ent = []
            tag = re.sub('[BI]-', '', labeldict['tag'])
            ent.append([indices, tag, labeldict['word']])
            n = min(len(outputlabels)-1, index+1)
            if not n == index:
                while re.sub('[BI]-', '', outputlabels[n]['tag']) == tag:
                    ent.append([tokenoffsets[n], tag, outputlabels[n]['word']])
                    n += 1
                    skip += 1
                    if n == len(outputlabels):
                        break
            entities.append(ent)
        index += 1
        for i in range(skip):
            index += 1
            next(zipiter)

    hardcoded_taClassRefMap = { # may want to not hard-code this...
        'PER': 'http://dbpedia.org/ontology/Person',
        'LOC': 'http://dbpedia.org/ontology/Location',
        'ORG': 'http://dbpedia.org/ontology/Organisation',
        'MISC': 'http://dbpedia.org/ontology/Miscellaneous' # not sure if this one actually exists
    }
    ner_annotations = {'PER': [], 'LOC': [], 'ORG': [], 'MISC': []}
    for ent in entities:
        tag = list(set([x[1] for x in ent]))[0]
        beginIndex = ent[0][0][0]
        endIndex = ent[-1][0][1]
        anchorOf = inp[beginIndex:endIndex]
        annot_dict = {
          "start": beginIndex,
          "end": endIndex,
          "features": {
             "nif:anchorOf": anchorOf,
             "itsrdf:taClassRef": hardcoded_taClassRefMap[tag]
             #"itsrdf:taIdentRef": "dbo:Berlin"
          }
        }
        ner_annotations[tag].append(annot_dict)
    #nifmodel.addEntities(outputlabels)
    return ner_annotations
    
def eval_conll(testdataloc, modelname):

    from bert import Ner
    model = Ner(os.path.join(OUTPUT_DIR, modelname))

    conlltokens = []
    for line in open(testdataloc).readlines():
        if not re.match('^$', line):
            tok = line.split()[0]
            label = line.split()[-1]
            conlltokens.append((tok, label))
    
    pretokenized = True
    out = model.predict(' '.join([x[0] for x in conlltokens]), pretokenized)

    actual = [x[1] for x in conlltokens]
    predicted = [x['tag'] for x in out]

    from sklearn.metrics import precision_score
    from sklearn.metrics import recall_score
    from sklearn.metrics import f1_score

    micro_precision = precision_score(actual, predicted, average='micro')
    micro_recall = recall_score(actual, predicted, average='micro')
    micro_f1 = f1_score(actual, predicted, average='micro')

    print('Precision (micro):', micro_precision)
    print('Recall (micro):   ', micro_recall)
    print('F1 (micro):       ', micro_f1)

    macro_precision = precision_score(actual, predicted, average='macro')
    macro_recall = recall_score(actual, predicted, average='macro')
    macro_f1 = f1_score(actual, predicted, average='macro')

    print('Precision (macro):', macro_precision)
    print('Recall (macro):   ', macro_recall)
    print('F1 (macro):       ', macro_f1)

    weighted_precision = precision_score(actual, predicted, average='weighted')
    weighted_recall = recall_score(actual, predicted, average='weighted')
    weighted_f1 = f1_score(actual, predicted, average='weighted')

    print('Precision (weighted):', weighted_precision)
    print('Recall (weighted):   ', weighted_recall)
    print('F1 (weighted):       ', weighted_f1)
    

def main():

    ### training ###
    traindataloc = '/home/peter/various/bertner/train.dummy.conll'
    """
    Example of required training data format
    (most important is that word is first column, label is last column):
    Der ART _ O
    Name NN _ O
    Maria NE _ I-LOC
    Grün NE _ I-LOC
    leitet VVFIN _ O
    sich PRF _ O
    von APPR _ O
    der ART _ O
    baumbestandenen ADJA _ O
    Umgebung NN _ O
    ab PTKVZ _ O
    . $. _ O
    """
    modelname = 'en-dummy'
    #train(traindataloc, modelname) # probably not worth while creating this for flask/api stuff, better to keep as back-end/background task
    modelname = 'wikiner-en'
    
    ### predicting ###
    inp = """
    Ich bin in berlin, julian ist in madrid.   

    Kommt dir Julian Moreno Schneider bekannt   vor?
    Und was mit Georg Rehm?"""
    inp = 'Kennst du Ela Elsholz?'
    inp = 'In Sachsen und Brandenburg hat die CDU bei den Wahlen stark verloren, vor allem an die AfD. Schleswig-Holsteins Ministerpräsident Daniel Günther spricht von einem "Alarmsignal".'
    informat = 'txt'
    outformat = 'turtle'
    #inp = 'I want to go to Berlin this summer. Do you know who Daniel Guerin is?'
    result = predict(inp, informat, outformat)
    print(result)

    

    ### evaluating ###
    # see above for sample of conll formatted data
    #testdataloc = '/home/peter/various/bertner/test.dummy.conll'
    #eval_conll(testdataloc, modelname)

    # TODO: expand nif compliancy (only turtle supported as of yet, and very little nifmodel options)
    # 
    
if __name__ == '__main__':

    main()
