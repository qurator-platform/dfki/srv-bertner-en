from rdflib import Namespace, Graph, URIRef, XSD, RDF, Literal, plugin
#from rdflib.serializer import Serializer
from rdflib.plugin import Serializer, register
import time
import tempfile
import sys
import os
import re
import utils
register('json-ld', Serializer, 'rdflib_jsonld.serializer', 'JsonLDSerializer')

class Nif():

    def __init__(self):
        self.NIF = Namespace('http://persistence.uni-leipzig.org/nlp2rdf/ontologies/nif-core#')
        self.ITSRDF = Namespace('http://www.w3.org/2005/11/its/rdf#')
        self.quratorns = Namespace("http://qurator.ai")
        self.model = Graph()

    def replaceDoubleQuotes(self):

        isstruri = self.isstruri
        isstr = self.extractIsString()
        #self.model.remove((isstruri, RDF.type, Literal(isstr)))
        self.model.add((isstruri, RDF.type, Literal(re.sub('"', '\'', isstr))))
        

    def initNifModelFromString(self, isString):

        isString = re.sub('"', '\'', isString)
        self.model.bind('nif', self.NIF)
        self.model.bind('itsrsdf', self.ITSRDF)
        textlen = len(isString)
        self.docuri = URIRef('%s/documents/%s' % (self.quratorns, str(time.time())))
        self.isstruri = URIRef('%s#char=%i,%i' % (self.docuri, 0, textlen))
        self.model.add((self.isstruri, RDF.type, self.NIF.Context))
        self.model.add((self.isstruri, RDF.type, self.NIF.String))
        self.model.add((self.isstruri, self.NIF.isString, Literal(isString)))
        self.model.add((self.isstruri, self.NIF.beginIndex, Literal(0, datatype=XSD.nonNegativeInteger)))
        self.model.add((self.isstruri, self.NIF.endIndex, Literal(textlen, datatype=XSD.nonNegativeInteger)))

    
    def extractIsString(self):

        isString = None
        if not (None, self.NIF.isString, None) in self.model:
            sys.stderr.write('ERROR: isString not found in nifstring. Not returning anything.\n')
        else:
            for s, p, o in self.model.triples((None, self.NIF.isString, None)): # should be only one, but found no way of getting it directly from generator object
                isString = str(o)
        return isString

    def parseModelFromString(self, nifstring, informat):

        # really stupid: there does not seem to be a way to rdflib.Graph().parse from string, so have to write to tempfile here
        fd, fn = tempfile.mkstemp()
        f = os.fdopen(fd, 'w')
        f.write(nifstring)
        f.close()
        if informat in ['turtle', 'xml', 'json-ld']:
            #self.model = Graph().parse(fn, format='n3')
            self.model = Graph().parse(fn, format=informat)
            subjects = set([re.sub('#char=.*$', '', x) for x in self.model.subjects()])
            rawsubjects = set([x for x in self.model.subjects()])
            if len(subjects) < 1:
                sys.stderr.write('ERROR: Could not find unique document uri. Very hacky way of getting this, which obviously failed at this point. Killing now.\n')
                sys.exit()
            self.docuri = list(subjects)[0]
            self.isstruri = list(rawsubjects)[0]
            self.replaceDoubleQuotes()

        else:
            sys.stderr.write('ERROR: Input format "%s" not supported. Not returning anything.\n' % informat)

        os.remove(fn)

    def addEntityLabelVocab(self, possible_labels):

        # taking out 'O' label, as this stand for other, not interesting.
        # also taking out BERT additions ([CLS] and [SEP], although these probably don't occur in the output)
        self.entityLabelVocab = [x for x in possible_labels if not re.match('O', x) and not re.match('\[CLS\]', x) and not re.match('\[SEP\]', x)]
        
        
    def addEntities(self, labels):

        tokenoffsets = utils.addOffsets(self.extractIsString())
        print(labels)
        assert len(tokenoffsets) == len(labels), "ERROR: lengths of output labels (%i) and token indices (%i) do not match" % (len(labels), len(tokenoffsets))


        # deciding to join together consecutive labels of the same type here. Since I haven't really seen any tags starting with B-, only I- ones (also entity-initially), feel that I can't really trust the BIO scheme for proper MWU entity handling

        entities = []
        zipiter = iter(zip(tokenoffsets, labels))
        index = 0
        for tupl in zipiter:
            indices, labeldict = tupl[0], tupl[1]
            skip = 0
            if labeldict['tag'] in self.entityLabelVocab:
                ent = []
                tag = re.sub('[BI]-', '', labeldict['tag'])
                ent.append([indices, tag, labeldict['word']])
                n = min(len(labels)-1, index+1)
                if not n == index:
                    while re.sub('[BI]-', '', labels[n]['tag']) == tag:
                        ent.append([tokenoffsets[n], tag, labels[n]['word']])
                        n += 1
                        skip += 1
                        if n == len(labels):
                            break
                entities.append(ent)
            index += 1
            for i in range(skip):
                index += 1
                next(zipiter)

        hardcoded_taClassRefMap = { # may want to not hard-code this...
            'PER': 'http://dbpedia.org/ontology/Person',
            'LOC': 'http://dbpedia.org/ontology/Location',
            'ORG': 'http://dbpedia.org/ontology/Organisation',
            'MISC': 'http://dbpedia.org/ontology/Miscellaneous' # not sure if this one actually exists
        }
        for ent in entities:
            tag = set([x[1] for x in ent])
            assert len(tag) == 1, "ERROR: More than one tag type assigned to entity '%s' (%s)" % (' '.join([x[2] for x in ent]), tag)
            beginIndex = ent[0][0][0]
            endIndex = ent[-1][0][1]
            isstring = self.extractIsString()
            anchorOf = isstring[beginIndex:endIndex]
            rc = self.isstruri
            annuri = URIRef("%s#char=%i,%i" % (self.docuri, beginIndex, endIndex))
            self.model.add((annuri, RDF.type, self.NIF.String))
            self.model.add((annuri, self.NIF.anchorOf, Literal(anchorOf)))
            self.model.add((annuri, self.NIF.beginIndex, Literal(beginIndex, datatype=XSD.nonNegativeInteger)))
            self.model.add((annuri, self.NIF.endIndex, Literal(endIndex, datatype=XSD.nonNegativeInteger)))
            self.model.add((annuri, self.ITSRDF.taClassRef, Literal(hardcoded_taClassRefMap[list(tag)[0]])))
        
        
        

if __name__ == '__main__':

    n = Nif()
    inp = """
    Ich bin in Berlin, Julian ist in Madrid.   

    Kommt dir Julian Moreno Schneider bekannt   vor?
    Und was mit "Georg Rehm"?"""
    n.initNifModelFromString(inp)
    
    
    #ser = n.model.serialize(format='turtle')
    #print(ser.decode('utf-8'))
    #sys.exit()
    samplenif = """
@prefix itsrsdf: <http://www.w3.org/2005/11/its/rdf#> .
@prefix nif: <http://persistence.uni-leipzig.org/nlp2rdf/ontologies/nif-core#> .
@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .
@prefix xml: <http://www.w3.org/XML/1998/namespace> .
@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .

<http://qurator.ai/documents/1566481330.803265#char=0,130> a nif:Context,
        nif:String ;
    nif:beginIndex "0"^^xsd:nonNegativeInteger ;
    nif:endIndex "132"^^xsd:nonNegativeInteger ;
    nif:isString \"""
    Ich bin in Berlin, Julian ist in Madrid.   

    Kommt dir Julian Moreno Schneider bekannt   vor?
    Und was mit "Georg Rehm"?\""" .

<http://qurator.ai/documents/1566481330.803265#char=119,129> a nif:String ;
    nif:anchorOf "Georg Rehm" ;
    nif:beginIndex "120"^^xsd:nonNegativeInteger ;
    nif:endIndex "130"^^xsd:nonNegativeInteger ;
    itsrsdf:taClassRef "http://dbpedia.org/ontology/Person" .

<http://qurator.ai/documents/1566481330.803265#char=16,22> a nif:String ;
    nif:anchorOf "Berlin" ;
    nif:beginIndex "16"^^xsd:nonNegativeInteger ;
    nif:endIndex "22"^^xsd:nonNegativeInteger ;
    itsrsdf:taClassRef "http://dbpedia.org/ontology/Location" .

<http://qurator.ai/documents/1566481330.803265#char=38,44> a nif:String ;
    nif:anchorOf "Madrid" ;
    nif:beginIndex "38"^^xsd:nonNegativeInteger ;
    nif:endIndex "44"^^xsd:nonNegativeInteger ;
    itsrsdf:taClassRef "http://dbpedia.org/ontology/Location" .

<http://qurator.ai/documents/1566481330.803265#char=64,87> a nif:String ;
    nif:anchorOf "Julian Moreno Schneider" ;
    nif:beginIndex "64"^^xsd:nonNegativeInteger ;
    nif:endIndex "87"^^xsd:nonNegativeInteger ;
    itsrsdf:taClassRef "http://dbpedia.org/ontology/Person" .



   
    """
    informat = 'turtle'
    n2 = Nif()
    nifmodel2 = n2.parseModelFromString(samplenif, informat)

    
    isstr = n.extractIsString()
    print('isstr here:', isstr)

    # this would be the result of sending isstr to ner.predict:
    labels = [{'word': 'Ich', 'tag': 'O', 'confidence': 0.999998927116394}, {'word': 'bin', 'tag': 'O', 'confidence': 0.9999996423721313}, {'word': 'in', 'tag': 'O', 'confidence': 0.9999994039535522}, {'word': 'Berlin', 'tag': 'I-LOC', 'confidence': 0.9869426488876343}, {'word': ',', 'tag': 'O', 'confidence': 0.9999994039535522}, {'word': 'Julian', 'tag': 'O', 'confidence': 0.999685525894165}, {'word': 'ist', 'tag': 'O', 'confidence': 0.9999994039535522}, {'word': 'in', 'tag': 'O', 'confidence': 0.9999966621398926}, {'word': 'Madrid', 'tag': 'I-LOC', 'confidence': 0.9996399879455566}, {'word': '.', 'tag': 'O', 'confidence': 0.9999996423721313}, {'word': 'Kommt', 'tag': 'O', 'confidence': 0.999992847442627}, {'word': 'dir', 'tag': 'O', 'confidence': 0.9999990463256836}, {'word': 'Julian', 'tag': 'I-PER', 'confidence': 0.9999823570251465}, {'word': 'Moreno', 'tag': 'I-PER', 'confidence': 0.9999850988388062}, {'word': 'Schneider', 'tag': 'I-PER', 'confidence': 0.9999650716781616}, {'word': 'bekannt', 'tag': 'O', 'confidence': 0.9999996423721313}, {'word': 'vor', 'tag': 'O', 'confidence': 0.999998927116394}, {'word': '?', 'tag': 'O', 'confidence': 0.9999994039535522}, {'word': 'Und', 'tag': 'O', 'confidence': 0.9999994039535522}, {'word': 'was', 'tag': 'O', 'confidence': 0.9999985694885254}, {'word': 'mit', 'tag': 'O', 'confidence': 0.9999994039535522}, {'word': '\'', 'tag': 'O', 'confidence': 0.9999994039535522}, {'word': 'Georg', 'tag': 'I-PER', 'confidence': 0.9999886751174927}, {'word': 'Rehm', 'tag': 'I-PER', 'confidence': 0.9999948740005493}, {'word': '\'', 'tag': 'O', 'confidence': 0.9999994039535522}, {'word': '?', 'tag': 'O', 'confidence': 0.9999996423721313}]

    possible_labels = ["O", "B-MISC", "I-MISC",  "B-PER", "I-PER", "B-ORG", "I-ORG", "B-LOC", "I-LOC", "[CLS]", "[SEP]"]
    n.addEntityLabelVocab(possible_labels)

    
    n.addEntities(labels) # directly working on the outputformat from ner.predict (a list with dictionaries for indiv words)
    
    
    ser = n.model.serialize(format='json-ld') # turtle, try rdfxml, jsonld (https://ropensci.github.io/rdflib/reference/rdf_serialize.html)
    # working so far; turtle, xml, json-ld. For json-ld, do a pip3 install rdflib-jsonld

    
    print(ser.decode('utf-8'))

    # TODO: add more formats (other than just turtle)
    
